import CredentialsProvider from "next-auth/providers/credentials";
import NextAuth from "next-auth";
import { cookies } from "next/headers";
import { encode } from "base-64";

const handler = NextAuth({
  pages: {
    signIn: "/login",
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: {},
        password: {},
      },
      async authorize(credentials) {
        if (!credentials) {
          return null;
        }

        try {
          const basicAuth = "Basic " + encode(credentials.email + ":" + credentials.password);
          const response = await fetch(
            `${process.env.NEXT_PUBLIC_API_KEY}Users/authenticate`,
            {
              method: "POST",
              headers: {
                Authorization: basicAuth,
              }    
            }
          );
          const authData = await response.json();
          if (authData.success !== true) {
            return null;
          }
          cookies().set("jwt", authData?.results?.token);
          return authData;
        } catch (error) {
          return null;
        }
      },
    }),
  ],
});

export { handler as GET, handler as POST };
