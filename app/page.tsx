import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";
import { FormCreateEmployee } from "@/components/forms/employee/form_create_employee";
import { FormUpdateEmployee } from "@/components/forms/employee/form_update_employee";
import { FormForgotPassword } from "@/components/forms/forgot_password/form_forgot_password";
import { FormLogin } from "@/components/forms/login/form_login";


export default async function Home() {
  const session = await getServerSession();
  if(!session){
    redirect("/login");
  }
  return (
    <main className="w-full min-h-screen flex items-center justify-center">
      <h1 className="text-7xl underline">Inicitial Page</h1>
    </main>
  );
}
