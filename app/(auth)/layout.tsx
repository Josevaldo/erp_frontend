export default function AuthLayout({
    children,
      }: Readonly<{
        children: React.ReactNode;
      }>) {
        return (
          <div className="bg-slate-300 w-full min-h-screen">{children}</div>
        );
      }