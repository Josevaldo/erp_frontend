import { FormForgotPassword } from "@/components/forms/forgot_password/form_forgot_password";

export default function ForgotPassword(){
    return(
        <div className="w-full min-h-screen flex items-center justify-center">
            <FormForgotPassword/>
        </div>
    )
}