import { FormLogin } from "@/components/forms/login/form_login";

export default function Login(){
    return(
        <div className="w-full min-h-screen flex items-center justify-center">
            <FormLogin/>
        </div>
    )
}