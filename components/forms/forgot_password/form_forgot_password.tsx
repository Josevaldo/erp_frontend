"use client"
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";
export const FormForgotPassword = () => {
  return (
    <div className="bg-[#F2F3F5] flex flex-col gap-5 w-full max-w-lg mx-auto items-center justify-center shadow-md py-6 px-4 rounded-md">
      <h1 className="text-3xl font-bold">Esqueci-me da palavra-passe</h1>
      <p className="w-full text-justify px-2 text-[#4F4F4F] text-sm">
        Enviar-lhe-emos uma ligação para o repor. Introduza o seu endereço de
        correio eletrónico utilizado para GSC
      </p>
      <form className="w-full flex flex-col gap-2 mt-2">
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="email">Email address</Label>
          <Input type="email" id="email" placeholder="Enter Your Email" />
        </div>
        <Button className="bg-[#3361FF] hover:bg-[#7e9cff]">Continuar</Button>
      </form>
    </div>
  );
};
