"use client";

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { FaApple } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa6";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { useForm, useFormState } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import {signIn} from "next-auth/react"

const dateLoginSchema = z.object({
  email: z.string().email(),
  password: z.string().min(5),
});

export const FormLogin = () => {
  type DateLoginSchema = z.infer<typeof dateLoginSchema>;

  const { register, handleSubmit, formState } = useForm<DateLoginSchema>({
    resolver: zodResolver(dateLoginSchema),
  });

  const { errors } = formState;

  function handleLoginUser(data: DateLoginSchema) {
    signIn("credentials",{
      ...data,
      callbackUrl:"/"
    })
  }

  return (
    <div className="max-w-4xl w-full mx-auto grid grid-cols-1 sm:grid-cols-2 shadow-md">
      {/* Img of Enterprise */}
      <div className="bg-imgLogin p-5 hidden sm:grid bg-cover bg-center"></div>
      <div className="bg-[#F2F3F5] p-5 flex flex-col gap-5 justify-center items-center">
        <Avatar>
          <AvatarImage src="https://github.com/shadcn.png" />
          <AvatarFallback>CN</AvatarFallback>
        </Avatar>
        <h1 className="text-4xl font-bold text-gray-700">Log In</h1>

        <div className="max-w-64 w-full flex flex-col gap-3">
          <div className="w-full py-2 cursor-pointer flex gap-3 items-center justify-center bg-white shadow-md rounded-md">
            <FaApple /> <p>Continue with Apple</p>
          </div>
          <div className="w-full py-2 cursor-pointer flex gap-3 items-center justify-center bg-white shadow-md rounded-md">
            <FaFacebookF /> <p>Continue with Facebook</p>
          </div>
        </div>

        <p className="text-1xl font-medium text-gray-400">Or with Email</p>

        <form
          onSubmit={handleSubmit(handleLoginUser)}
          className="flex flex-col gap-4 max-w-64 w-full"
        >
          <div className="grid w-full items-center gap-1.5">
            <Label htmlFor="email">Email</Label>
            <Input
              type="email"
              id="email"
              placeholder="Enter Your Email"
              {...register("email")}
            />
            {errors.email && (
              <p className="text-red-500">{errors.email.message}</p>
            )}
          </div>
          <div className="grid w-full  items-center gap-1.5">
            <Label htmlFor="password">Password</Label>
            <Input
              type="password"
              id="password"
              placeholder="*"
              {...register("password")}
            />
            {errors.password && (
              <p className="text-red-500">{errors.password.message}</p>
            )}
          </div>
          <Button className="bg-[#3361FF] hover:bg-[#7e9cff]" type="submit">Login</Button>
        </form>
        <div className="flex flex-col gap-1 justify-center items-center">
          <Link
            href="/forgot_password"
            className="text-blue-500  underline underline-offset-2 hover:no-underline"
          >
            Forgot password?
          </Link>
        </div>
      </div>
        
    </div>
  );
};
