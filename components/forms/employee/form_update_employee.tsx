import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Checkbox } from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
export const FormUpdateEmployee = ()=>{
    return(
        <div className="flex flex-col gap-5 p-5 w-full max-w-lg shadow-md rounded-md">
      <h1 className="text-3xl font-bold">Actualização de Funcionário</h1>
      <form className="flex flex-col gap-7">
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="name">Nome</Label>
          <Input type="text" id="name" placeholder="Enter your name" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="cpf">CPF</Label>
          <Input type="text" id="cpf" placeholder="CPF" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="birth">Data de aniversário</Label>
          <Input type="text" id="birth" placeholder="20/12/2005" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="salary">Salário base</Label>
          <Select>
            <SelectTrigger className="w-full" id="salary">
              <SelectValue placeholder="Selecione" />
            </SelectTrigger>
            <SelectContent>
              <SelectItem value="light">Light</SelectItem>
              <SelectItem value="dark">Dark</SelectItem>
              <SelectItem value="system">System</SelectItem>
            </SelectContent>
          </Select>
        </div>
        <Button className="bg-[#3361FF] hover:bg-[#7e9cff]">Actualizar</Button>
      </form>
    </div>
    )
}