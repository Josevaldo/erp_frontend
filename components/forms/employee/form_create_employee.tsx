import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Checkbox } from "@/components/ui/checkbox";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
export const FormCreateEmployee = () => {
  return (
    <div className="flex flex-col gap-5 p-5 w-full max-w-lg shadow-md rounded-md">
      <h1 className="text-3xl font-bold">Criação de Funcionário</h1>
      <form className="flex flex-col gap-7">
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="name">Nome</Label>
          <Input type="text" id="name" placeholder="Enter your name" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="cpf">CPF</Label>
          <Input type="text" id="cpf" placeholder="CPF" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="birth">Data de aniversário</Label>
          <Input type="text" id="birth" placeholder="20/12/2005" />
        </div>
        <div className="grid w-full items-center gap-1.5">
          <Label htmlFor="salary">Salário base</Label>
          <Select>
            <SelectTrigger className="w-full" id="salary">
              <SelectValue placeholder="Selecione" />
            </SelectTrigger>
            <SelectContent>
              <SelectItem value="light">Light</SelectItem>
              <SelectItem value="dark">Dark</SelectItem>
              <SelectItem value="system">System</SelectItem>
            </SelectContent>
          </Select>
        </div>
        <div className="flex items-center space-x-2">
          <Checkbox id="terms" />
          <label
            htmlFor="terms"
            className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
          >
            Aceitar termos e condições!
          </label>
        </div>
        <Button className="bg-[#3361FF] hover:bg-[#7e9cff]">Criar</Button>
      </form>
    </div>
  );
};
